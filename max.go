package main

import "fmt"

func main() {
	var er int
	fmt.Print("enter the array size =  ")
	fmt.Scan(&er)

	fmt.Print("enter the no =  ")

	array := make([]int, er)
	for i := 0; i < er; i++ {
		fmt.Scan(&array[i])
	}
	max := array[0]

	for i := 1; i < er; i++ {

		if max < array[i] {

			max = array[i]
		}
	}

	fmt.Printf("\nMax element is : %d", max)

}
