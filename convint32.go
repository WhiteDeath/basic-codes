package main

import (
	"fmt"
	"reflect"
	"sort"
)

//sorting int32 type
func main() {

	s := []int32{5, 2, 6, 3, 1, 4}

	sort.Slice(s, func(i, j int) bool {
		return s[i] < s[j]
	})
	fmt.Println(s)
	fmt.Println(reflect.TypeOf(s))
}
