package main

import "fmt"

func main() {
	// Allocate memory for a new string object
	s := "hello, world"

	// The variable s is still in scope, so the string object it references is considered reachable
	fmt.Println(s)

	// Once the variable s goes out of scope, the string object it references is no longer reachable
	// and will be eligible for garbage collection
}
