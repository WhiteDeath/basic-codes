package GetallBooks

import (
	"encoding/json"
	"jwt.go/Authendpoint"
	"net/http"
)

func GetAllBookHandler(w http.ResponseWriter, r *http.Request) {
	err := Authendpoint.ValidateToken(w, r)
	if err == nil {
		w.Header().Set("Content-Type", "application/json")
		books := getAllBook()
		json.NewEncoder(w).Encode(books)
	}
}

func getAllBook() []Book {
	return []Book{
		Book{
			Name:   "Book1",
			Author: "Author1",
		},
		Book{
			Name:   "Book2",
			Author: "Author2",
		},
		Book{
			Name:   "Book3",
			Author: "Author3",
		},
	}
}

type Book struct {
	Name   string
	Author string
}
