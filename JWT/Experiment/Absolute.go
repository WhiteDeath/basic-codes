package main

import (
	"fmt"
	"math"
)

func main() {

	n := 3
	var x = [3][3]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
	fmt.Println(x)
	var d1 []int
	var d2 []int
	var a = 0
	var b = 0
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			fmt.Print(" ")
			if i == j {
				fmt.Print(x[i][j])
				d1 = append(d1, x[i][j])
				a += x[i][j]
			}
			if (i + j) == n-1 {
				fmt.Print(x[i][j])
				d2 = append(d2, x[i][j])
				b += x[i][j]

			}
			fmt.Print(" ")
		}
		fmt.Println("")
	}
	fmt.Println("d1 ", d1)

	fmt.Println("d2 ", d2)

	fmt.Println(a, b)

	diff := (a - b)
	fmt.Println(diff)
	m := math.Abs(float64(diff))
	fmt.Println(int32(m))

}

//func DigonalDiff(arr [][]int32) int32 {
//
//}
