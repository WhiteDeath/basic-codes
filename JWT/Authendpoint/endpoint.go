package Authendpoint

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"net/http"
	"time"
)

// store all user
var userMap = map[string]string{
	"golinuxcloud": "testpw",
	"testUser1":    "testUser1",
	"testUser2":    "testUser2",
}

// USer type
type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func LoginHandler(writer http.ResponseWriter, request *http.Request) {
	switch request.Method {
	case "POST":
		var user User

		// decode the request body into the struct, If error, respond to the client with the error message and a 400 status code.
		err := json.NewDecoder(request.Body).Decode(&user)
		if err != nil {
			fmt.Println(user)
			fmt.Fprintf(writer, "invalid body")
		}

		if userMap[user.Username] == "" || userMap[user.Username] != user.Password {
			fmt.Fprintf(writer, "can not authenticate this user")
		}

	case "GET":
		fmt.Fprintf(writer, "only POST methods is allowed.")
	}
}

///Generating Jwt

var sampleSecretKey = []byte("GoLinuxCloudKey")

func GenerateJWT(username string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["username"] = username
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

	tokenString, err := token.SignedString(sampleSecretKey)

	if err != nil {
		fmt.Errorf("Something Went Wrong: %s", err.Error())
		return "", err
	}
	return tokenString, nil
}

//////Validating token

func ValidateToken(w http.ResponseWriter, r *http.Request) (err error) {

	if r.Header["Token"] == nil {
		fmt.Fprintf(w, "can not find token in header")
		return
	}

	token, err := jwt.Parse(r.Header["Token"][0], func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error in parsing")
		}
		return sampleSecretKey, nil
	})

	if token == nil {
		fmt.Fprintf(w, "invalid token")
	}

	////extract the claim

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		fmt.Fprintf(w, "couldn't parse claims")
		return errors.New("Token error")
	}

	exp := claims["exp"].(float64)
	if int64(exp) < time.Now().Local().Unix() {
		fmt.Fprintf(w, "token expired")
		return errors.New("Token error")
	}

	return nil
}
