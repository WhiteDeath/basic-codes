package main

import (
	"fmt"
	"jwt.go/Authendpoint"
	"jwt.go/GetallBooks"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "This is GolinuxCloud's http server")
	})
	http.HandleFunc("/login", Authendpoint.LoginHandler)
	http.HandleFunc("/getallbooks", GetallBooks.GetAllBookHandler)
	http.ListenAndServe(":8080", nil)

}
