package main

import "fmt"

func main() {

	// I'm using range and for loop for adding the number
	array := []int{4, 5, 7, 14, 34}
	sum := 0

	for _, num := range array {
		sum += num
	}
	fmt.Println("sum:", sum)

}
