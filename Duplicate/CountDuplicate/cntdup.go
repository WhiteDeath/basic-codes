package main

import (
	"fmt"
)

func findGreat(array []int) int {
	greatest := 0
	for i := 0; i < len(array); i++ {
		if array[i] > greatest {
			greatest = array[i]
		}
	}
	return greatest + 1
}

func findDup(array []int) []int {
	dup := make([]int, findGreat(array))

	for i := 0; i < len(array); i++ {
		dup[array[i]] += 1
	}
	return dup
}
func main() {
	array := []int{8, 7, 5, 0, 0, 1, 2, 2, 8, 3, 4, 3, 4, 1, 6, 6, 7, 0, 4, 8}
	fmt.Println(array)
	fmt.Println(findDup(array))

	for i, v := range findDup(array) {
		fmt.Printf("value %v, repetitions %v\\n", i, v)
	}
}
