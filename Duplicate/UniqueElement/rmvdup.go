package main

import (
	"fmt"
)

func uniqueeNonDuplicate(array []int) int {
	unique := 0
	for i := 0; i < len(array); i++ {
		unique ^= array[i]

	}
	return unique
}

func main() {
	// 5 is the unique element here
	array := []int{8, 7, 5, 0, 0, 1, 2, 2, 8, 3, 4, 3, 4, 1, 6, 6, 7}

	fmt.Println(uniqueeNonDuplicate(array))
}

