package main

import (
	"fmt"
)

func keepOne(array []int) []int {
	keepOne := make([]int, 0)
	for i := 0; i < len(array); i++ {
		if elementExists(keepOne, array[i]) == false {
			keepOne = append(keepOne, array[i])
		}
	}
	return keepOne
}

func elementExists(haystack []int, needle int) bool {
	for _, v := range haystack {
		if v == needle {
			return true
		}
	}
	return false
}

func main() {
	array := []int{8, 7, 5, 0, 0, 1, 2, 2, 8, 3, 4, 3, 4, 1, 6, 6, 7}
	fmt.Println(array)
	fmt.Println(keepOne(array))
}
