package main

import "fmt"

func findDupMap(array []int) map[int]int {

	dup := make(map[int]int)

	for i := 0; i < len(array); i++ {
		dup[array[i]] += 1
	}
	return dup
}
 
func main() {
	array := []int{23, 8, 7, 5, 0, 0, 1, 2, 2, 8, 3, 4, 3, 4, 1, 6, 6, 7}

	fmt.Println(findDupMap(array))
	fmt.Printf("Element %v repeated %v\\n", 23, findDupMap(array)[23])

	fmt.Println()
	for i, v := range findDupMap(array) {
		fmt.Printf("Element %v repeated %v\\n", i, v)
	}

}
