package Pyramid

import (
	"fmt"
)

func Pyamid_Number(rows int) {
	var k, temp, temp1 int
	fmt.Print("Enter number of rows :")
	fmt.Scan(&rows)

	for i := 1; i <= rows; i++ {

		for j := 1; j <= rows-i; j++ {
			fmt.Print("  ")
			temp++
		}
		for {
			if temp <= rows-1 {
				fmt.Printf(" %d", i+k)
				temp++
			} else {
				temp1++
				fmt.Printf(" %d", (i + k - 2*temp1))
			}
			k++

			if k == 2*i-1 {
				break
			}

		}
		temp = 0
		temp1 = 0
		k = 0
		fmt.Println("")
	}

}

func Pyramid_(rows int) {

	//var rows int
	var k int = 0
	fmt.Print("Enter number of rows :")
	fmt.Scan(&rows)
	for i := 1; i <= rows; i++ {
		k = 0
		for space := 1; space <= rows-i; space++ {
			fmt.Print("  ")
		}
		for {
			fmt.Print("* ")
			k++
			if k == 2*i-1 {
				break
			}
		}
		fmt.Println("")
	}
}

func CuboidDraw(drawX, drawY, drawZ int) {
	fmt.Printf("Cuboid %d %d %d:\n", drawX, drawY, drawZ)
	CubeLine(drawY+1, drawX, 0, "+-")
	for i := 1; i <= drawY; i++ {
		CubeLine(drawY-i+1, drawX, i-1, "/ |")
	}
	CubeLine(0, drawX, drawY, "+-|")
	for i := 4*drawZ - drawY - 2; i > 0; i-- {
		CubeLine(0, drawX, drawY, "| |")
	}
	CubeLine(0, drawX, drawY, "| +")
	for i := 1; i <= drawY; i++ {
		CubeLine(0, drawX, drawY-i, "| /")
	}
	CubeLine(0, drawX, 0, "+-\n")
}

func CubeLine(n, drawX, drawY int, cubeDraw string) {
	fmt.Printf("%*s", n+1, cubeDraw[:1])
	for d := 9*drawX - 1; d > 0; d-- {
		fmt.Print(cubeDraw[1:2])
	}
	fmt.Print(cubeDraw[:1])
	fmt.Printf("%*s\n", drawY+1, cubeDraw[2:])
}
