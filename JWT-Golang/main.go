package main

import (
	"JWT-Golang/Server"
	"net/http"
)

func main() {
	http.HandleFunc("/login", Server.LoginHandler)
	http.HandleFunc("/getAllBooks", Server.GetAllBookHandler)
	http.ListenAndServe(":8080", nil)
}
