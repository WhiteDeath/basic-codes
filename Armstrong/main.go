/* A positive integer is called an Armstrong number of order n if
   the sum of cubes of each digits is equal to the number itself.
   153 = 1*1*1 + 5*5*5 + 3*3*3  // 153 is an Armstrong number.
*/

package main

import "fmt"

func main() {
	var n int
	var result = 0

	var v = n

	fmt.Println("Enter the number")
	fmt.Scanln(&n)
	for {
		remainder := v % 10
		result += remainder * remainder * remainder
		n /= 10

		if v == 0 {
			break // Break Statement used to stop the loop
		}
	}

	if result == n {
		fmt.Printf("%d is an Armstrong number.", n)
	} else {
		fmt.Printf("%d is not an Armstrong number.", n)
	}

	fmt.Println("")

}
